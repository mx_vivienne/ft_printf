/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_list1.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/31 01:36:39 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/10/31 15:59:14 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void *content)
{
	t_list	*elem;

	elem = (t_list*)malloc(sizeof(t_list));
	if (elem != NULL)
	{
		elem->content = content;
		elem->next = NULL;
	}
	return (elem);
}

int		ft_lstsize(t_list *lst)
{
	t_list	*elem;
	int		len;

	elem = lst;
	len = 0;
	while (elem != NULL)
	{
		elem = elem->next;
		len++;
	}
	return (len);
}

t_list	*ft_lstlast(t_list *lst)
{
	t_list	*elem;

	if (lst == NULL)
		return (NULL);
	elem = lst;
	while (elem->next != NULL)
		elem = elem->next;
	return (elem);
}
