/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_str3.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/30 02:32:16 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/11/22 17:31:46 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_isinset(const char *set, char c)
{
	size_t	offset;

	offset = 0;
	while (set[offset] != '\0')
	{
		if (set[offset] == c)
			return (1);
		offset++;
	}
	return (0);
}

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	offset;
	char			*str;

	if (s == NULL || f == NULL)
		return (NULL);
	offset = 0;
	str = (char*)malloc(ft_strlen(s) + 1);
	if (str != NULL)
	{
		while (s[offset] != '\0')
		{
			str[offset] = (*f)(offset, s[offset]);
			offset++;
		}
		str[offset] = '\0';
	}
	return (str);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	size_t	offset;
	size_t	size;
	char	*str;

	if (s1 == NULL || set == NULL)
		return (NULL);
	size = ft_strlen(s1) + 1;
	str = (char*)malloc(size);
	if (str != NULL)
	{
		*str = '\0';
		offset = 0;
		while (ft_isinset(set, s1[offset]))
		{
			if (s1[offset] == '\0')
				return (str);
			offset++;
		}
		offset = ft_strlcpy(str, s1 + offset, size) - 1;
		while (ft_isinset(set, str[offset]))
			offset--;
		str[offset + 1] = '\0';
	}
	return (str);
}
