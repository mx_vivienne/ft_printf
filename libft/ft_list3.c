/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_list3.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/31 01:40:16 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/11/18 16:12:41 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstiter(t_list *lst, void (*f)(void *))
{
	if (f == NULL)
		return ;
	while (lst != NULL)
	{
		(*f)(lst->content);
		lst = lst->next;
	}
}

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*new;
	t_list	*elem;
	t_list	*next;

	if (lst == NULL || f == NULL || *f == NULL || del == NULL)
		return (NULL);
	elem = ft_lstnew((*f)(lst->content));
	if (elem == NULL)
		return (NULL);
	new = elem;
	while (lst->next != NULL)
	{
		lst = lst->next;
		next = ft_lstnew((*f)(lst->content));
		if (next == NULL)
		{
			ft_lstclear(&new, del);
			return (NULL);
		}
		elem->next = next;
		elem = next;
	}
	return (new);
}
