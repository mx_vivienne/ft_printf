/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_convert.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/30 01:46:41 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/11/20 17:12:17 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_toupper(int c)
{
	if ('a' <= c && c <= 'z')
		return (c - 32);
	return (c);
}

int			ft_tolower(int c)
{
	if ('A' <= c && c <= 'Z')
		return (c + 32);
	return (c);
}

int			ft_atoi(const char *n)
{
	size_t			i;
	int				sign;
	unsigned int	val;

	i = 0;
	val = 0;
	sign = 1;
	while (n[i] == ' ' || n[i] == '\n' || n[i] == '\t'
			|| n[i] == '\f' || n[i] == '\r' || n[i] == '\v')
		i++;
	if (n[i] == '+' || n[i] == '-')
	{
		if (n[i] == '-')
			sign = -1;
		i++;
	}
	while (ft_isdigit(n[i]))
	{
		val = 10 * val + n[i] - '0';
		i++;
	}
	return ((int)(sign * val));
}

static void	ft_strrev(char *str)
{
	size_t	offset;
	size_t	len;
	char	temp;

	len = ft_strlen(str);
	if (len > 1)
	{
		offset = 0;
		while (offset < len / 2)
		{
			temp = str[offset];
			str[offset] = str[len - offset - 1];
			str[len - offset - 1] = temp;
			offset++;
		}
	}
}

char		*ft_itoa(int n)
{
	char	*str;
	long	abs;
	size_t	offset;

	str = (char*)ft_calloc(12, sizeof(char));
	if (str != NULL)
	{
		if (n < 0)
			abs = -1 * (long)n;
		else
			abs = (long)n;
		offset = 0;
		while (abs > 0 || offset == 0)
		{
			str[offset] = (char)(abs % 10) + '0';
			abs /= 10;
			offset++;
		}
		if (n < 0)
			str[offset] = '-';
		ft_strrev(str);
	}
	return (str);
}
