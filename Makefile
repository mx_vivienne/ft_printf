IDIR = include
SDIR = src
ODIR = $(SDIR)/obj
FTDIR = libft
LIBFT = $(FTDIR)/libft.a

_DEPS = ft_printf.h libft.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

SRC = ft_printf.c handle_conv.c misc1.c int1.c int2.c print_misc.c print_ints.c

_OBJ = $(SRC:.c=.o)
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

CFLAGS = -Wall -Werror -Wextra -I$(IDIR)
NAME = libftprintf.a

$(NAME): $(OBJ) $(LIBFT)
	cp $(LIBFT) $(NAME)
	ar rs $@ $(OBJ)

$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -c -o $@ $< $(CFLAGS)

$(LIBFT):
	make -C $(FTDIR) all

.PHONY: all bonus re clean fclean

all: $(NAME)

bonus: all

re: fclean all

clean:
	-rm -f $(OBJ)
	make -C $(FTDIR) clean

fclean:
	-rm -f $(NAME) $(OBJ)
	make -C $(FTDIR) fclean
