/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   handle_conv.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/06 15:27:15 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/02 12:40:26 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	handle_flags(char *start, t_conv *conv)
{
	size_t	offset;

	offset = 0;
	while (ft_isinset(FLAGS, start[offset]))
	{
		conv->flags |= 1 << (find_char(FLAGS, start[offset]));
		offset++;
	}
	return (offset);
}

size_t	handle_width(char *start, t_conv *conv, va_list args)
{
	size_t	offset;
	int		width;

	offset = 0;
	if ('1' <= *start && *start <= '9')
	{
		while (ft_isdigit(start[offset]))
		{
			conv->width *= 10;
			conv->width += (unsigned)(start[offset] - '0');
			offset++;
		}
	}
	if (*start == '*')
	{
		width = va_arg(args, int);
		conv->width = (size_t)width;
		if (width < 0)
		{
			conv->width = (size_t)(-1 * width);
			conv->flags |= 1 << (find_char(FLAGS, '-'));
		}
		offset++;
	}
	return (offset);
}

size_t	handle_precision(char *start, t_conv *conv, va_list args)
{
	size_t	offset;
	int		prec;

	if (*start != '.')
		return (0);
	offset = 1;
	conv->use_prec = 1;
	if (start[offset] == '*')
	{
		prec = va_arg(args, int);
		if (prec < 0)
			conv->use_prec = 0;
		else
			conv->prec = (size_t)prec;
		return (offset + 1);
	}
	conv->use_prec = !(start[offset] == '-');
	offset += (start[offset] == '-');
	while (ft_isdigit(start[offset]))
	{
		conv->prec *= 10;
		conv->prec += (unsigned)(start[offset] - '0');
		offset++;
	}
	return (offset);
}

size_t	handle_length(char *start, t_conv *conv)
{
	if (start[0] == 'h' || start[0] == 'l')
	{
		conv->length = start[0];
		if (start[1] == start[0])
		{
			conv->length *= -1;
			return (2);
		}
		return (1);
	}
	return (0);
}

size_t	handle_type(char *start, t_conv *conv)
{
	conv->type = *start;
	if (conv->use_prec && ft_isinset(NUMERIC, conv->type))
		conv->flags &= ~2ULL;
	return (1);
}
