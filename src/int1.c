/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   int1.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/10 17:03:37 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/09 19:42:49 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	write_ullong(t_conv conv, unsigned long long n,
		size_t radix, int inupper)
{
	char	digit;
	size_t	length;

	length = 0;
	if (n >= radix)
		length = write_ullong(conv, n / radix, radix, inupper);
	digit = (char)(n % radix) + '0';
	if (digit > '9')
	{
		if (inupper)
			digit += 7;
		else
			digit += 39;
	}
	write(conv.fd, &digit, 1);
	return (1 + length);
}

void	get_int(t_conv conv, va_list args, long long *num)
{
	if (conv.length == -1 * 'l')
		*num = va_arg(args, long long);
	else if (conv.length == 'l')
		*num = (long)va_arg(args, long);
	else if (conv.length == -1 * 'h')
		*num = (char)va_arg(args, int);
	else if (conv.length == 'h')
		*num = (short)va_arg(args, int);
	else
		*num = (int)va_arg(args, int);
}

void	get_uint(t_conv conv, va_list args, unsigned long long *num)
{
	if (conv.length == -1 * 'l')
		*num = (unsigned long long)va_arg(args, long long);
	else if (conv.length == 'l')
		*num = (unsigned long)va_arg(args, long);
	else if (conv.length == -1 * 'h')
		*num = (unsigned char)va_arg(args, int);
	else if (conv.length == 'h')
		*num = (unsigned short)va_arg(args, int);
	else
		*num = (unsigned)va_arg(args, int);
}

size_t	write_int(t_conv conv, va_list args, size_t radix, int isupper)
{
	long long	num;
	size_t		len;
	size_t		size;
	char		skip;
	char		use_sign;

	get_int(conv, args, &num);
	skip = (num == 0) && (conv.use_prec && !conv.prec);
	use_sign = (num < 0) || (conv.flags & 3 << 3);
	len = llong_len(num, radix);
	size = write_sign(conv, num < 0, conv.flags & 1 << 1);
	size += write_padding(conv, !skip * len, use_sign, 0);
	size += write_sign(conv, num < 0, !(conv.flags & 1 << 1));
	size += repeat_if(conv, '0', conv.prec - len,
			conv.use_prec && conv.prec > len);
	if (num >= 0 && !skip)
		size += write_ullong(conv, (unsigned long long)num, radix, isupper);
	else if (!skip)
		size += write_ullong(conv, (unsigned long long)(-1 * num),
				radix, isupper);
	return (size + write_padding(conv, !skip * len, use_sign, 1));
}

size_t	write_uint(t_conv conv, va_list args, size_t radix, int isupper)
{
	unsigned long long	num;
	size_t				len;
	size_t				size;
	char				skip;
	char				use_prefix;

	get_uint(conv, args, &num);
	len = ullong_len(num, radix);
	skip = (num == 0) && (conv.use_prec && !conv.prec);
	oct_prec(&conv, len, radix == 8 && num != 0);
	use_prefix = (conv.flags & 1) && (radix == 16) && (num != 0);
	size = 0;
	size = write_padding(conv, !skip * len, 2 * use_prefix, 0);
	size += prefix_if(conv, radix, isupper, use_prefix);
	size += repeat_if(conv, '0', conv.prec - len,
			conv.use_prec && conv.prec > len);
	if (!skip)
		size += write_ullong(conv, num, radix, isupper);
	return (size + write_padding(conv, !skip * len, 2 * use_prefix, 1));
}
