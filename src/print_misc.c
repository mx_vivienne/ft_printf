/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   print_misc.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/06 19:24:40 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/09 19:45:32 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t			print_unknown(t_conv conv, va_list args)
{
	unsigned char	c;

	(void)args;
	c = conv.type;
	write(conv.fd, &c, 1);
	return (1);
}

size_t			print_n(t_conv conv, va_list args)
{
	long long	*ll;
	long		*l;
	int			*i;
	short		*s;
	char		*c;

	ll = (long long*)va_arg(args, void*);
	l = (long*)ll;
	i = (int*)ll;
	s = (short*)ll;
	c = (char*)ll;
	if (conv.length == -1 * 'l')
		*ll = (long long)conv.written;
	else if (conv.length == 'l')
		*l = (long)conv.written;
	else if (conv.length == -1 * 'h')
		*c = (char)conv.written;
	else if (conv.length == 'h')
		*s = (short)conv.written;
	else
		*i = (int)conv.written;
	return (0);
}

size_t			print_char(t_conv conv, va_list args)
{
	unsigned char	c;
	size_t			size;

	c = (unsigned char)va_arg(args, int);
	size = write_padding(conv, 1, 0, 0);
	write(conv.fd, &c, 1);
	return (1 + size + write_padding(conv, 1, 0, 1));
}

size_t			print_ptr(t_conv conv, va_list args)
{
	unsigned long long	address;
	size_t				len;
	void				*ptr;
	int					left_adj;

	left_adj = conv.flags & (1 << 2);
	ptr = va_arg(args, void*);
	address = (unsigned long long)ptr;
	len = ullong_len(address, 16) + 2;
	len += repeat_if(conv, ' ', conv.width - len,
			!left_adj && conv.width > len);
	write(conv.fd, "0x", 2);
	write_ullong(conv, address, 16, 0);
	len += repeat_if(conv, ' ', conv.width - len,
			left_adj && conv.width > len);
	return (len);
}

size_t			print_str(t_conv conv, va_list args)
{
	unsigned char	*str;
	size_t			len;
	size_t			size;

	str = (unsigned char*)va_arg(args, char*);
	if (str == NULL)
		str = (unsigned char*)"(null)";
	len = ft_strlen((char*)str);
	if (conv.use_prec && (len > conv.prec))
		len = conv.prec;
	size = write_padding(conv, len, 0, 0);
	write(conv.fd, str, len);
	return (len + size + write_padding(conv, len, 0, 1));
}
