/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_printf.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/05 19:30:42 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/04/29 15:28:40 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t 	(*g_type_handlers[])(t_conv, va_list) = {
	&print_char, &print_str, &print_signed, &print_signed, &print_octal,
	&print_unsigned, &print_lowhex, &print_uphex, &print_ptr, &print_n
};

char	*g_known_types = TYPES;

int		ft_printf(const char *format, ...)
{
	va_list	ap;
	int		written;

	va_start(ap, format);
	written = ft_vdprintf(1, format, ap);
	va_end(ap);
	return (written);

}

int		ft_dprintf(int fd, const char *format, ...)
{
	va_list	ap;
	int		written;

	va_start(ap, format);
	written = ft_vdprintf(fd, format, ap);
	va_end(ap);
	return (written);
}

int		ft_vprintf(const char *format, va_list ap)
{
	return (ft_vdprintf(1, format, ap));
}

int		ft_vdprintf(int fd, const char *format, va_list ap)
{
	size_t	offset;
	size_t	written;

	offset = 0;
	written = 0;
	while (format[offset] != '\0')
	{
		if (format[offset] == '%')
		{
			offset++;
			offset += handle_conv((char*)format + offset, ap, &written, fd);
		}
		else
		{
			write(fd, format + offset, 1);
			written++;
			offset++;
		}
	}
	return (written);
}

size_t	handle_conv(char *start, va_list args, size_t *written, int fd)
{
	size_t	offset;
	t_conv	conv;

	conv = (t_conv){0, 0, 0, 0, 0, 0, *written, fd};
	offset = handle_flags(start, &conv);
	offset += handle_width(start + offset, &conv, args);
	offset += handle_precision(start + offset, &conv, args);
	offset += handle_length(start + offset, &conv);
	offset += handle_type(start + offset, &conv);
	if (ft_isinset(g_known_types, conv.type))
	{
		conv.written += (*g_type_handlers[
			ft_strchr(g_known_types, conv.type) - g_known_types])(conv, args);
	}
	else
		conv.written += print_unknown(conv, args);
	*written = conv.written;
	return (offset);
}
