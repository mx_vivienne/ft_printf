/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   misc1.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/11 21:04:17 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/09 19:39:25 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	repeat_if(t_conv conv, char c, size_t amount, int condition)
{
	size_t	offset;

	if (!condition)
		return (0);
	offset = 0;
	while (offset < amount)
	{
		write(conv.fd, &c, 1);
		offset++;
	}
	return (amount);
}

size_t	write_if(t_conv conv, char *s, int condition)
{
	size_t	len;

	if (!condition)
		return (0);
	len = ft_strlen(s);
	write(conv.fd, s, len);
	return (len);
}

size_t	write_padding(t_conv conv, size_t len, size_t extra, int rpad)
{
	int	left_adj;

	left_adj = conv.flags & (1 << 2);
	if (!rpad != !left_adj)
		return (0);
	if (conv.use_prec && !ft_isinset("pfFeE", conv.type))
	{
		if (ft_isinset(NUMERIC, conv.type))
		{
			if (conv.prec > len)
				len = conv.prec;
		}
		else if (conv.prec < len)
			len = conv.prec;
	}
	len += extra;
	if (conv.width > len)
	{
		if (!left_adj && (conv.flags & (1 << 1)))
			return (repeat_if(conv, '0', conv.width - len, 1));
		return (repeat_if(conv, ' ', conv.width - len, 1));
	}
	return (0);
}

ssize_t	find_char(const char *s, char c)
{
	ssize_t	offset;

	offset = 0;
	while (s[offset] != '\0')
	{
		if (s[offset] == c)
			return (offset);
		offset++;
	}
	return (-1);
}

size_t	write_ustr(t_conv conv, unsigned char *str, size_t len)
{
	if (conv.use_prec && (len > conv.prec))
		len = conv.prec;
	write(conv.fd, str, len);
	return (len);
}
