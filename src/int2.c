/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   int2.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.n>          +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/12/02 12:03:13 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/09 19:37:45 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	ullong_len(unsigned long long n, size_t radix)
{
	size_t	len;

	len = 1;
	while (n >= radix)
	{
		n /= radix;
		len++;
	}
	return (len);
}

size_t	llong_len(long long n, size_t radix)
{
	if (n >= 0)
		return (ullong_len((unsigned long long)n, radix));
	return (ullong_len((unsigned long long)(-1 * n), radix));
}

t_conv	*oct_prec(t_conv *conv, size_t len, int condition)
{
	if (condition && conv->flags & 1 &&
			(!conv->use_prec || conv->prec < len + 1))
	{
		conv->use_prec = 1;
		conv->prec = len + 1;
	}
	return (conv);
}

size_t	prefix_if(t_conv conv, size_t radix, int isupper, int cond)
{
	if (!cond)
		return (0);
	if (16 == radix)
		return (write_if(conv, "0X", isupper) +
				write_if(conv, "0x", !isupper));
	return (0);
}

size_t	write_sign(t_conv conv, int negative, int cond)
{
	char sign;

	if (!cond)
		return (0);
	if (negative)
		sign = '-';
	else if (conv.flags & 1 << 3)
		sign = '+';
	else if (conv.flags & 1 << 4)
		sign = ' ';
	else
		return (0);
	write(conv.fd, &sign, 1);
	return (1);
}
