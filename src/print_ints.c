/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   print_ints.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/09 20:18:03 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/02 12:31:53 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	print_signed(t_conv conv, va_list args)
{
	return (write_int(conv, args, 10, 0));
}

size_t	print_unsigned(t_conv conv, va_list args)
{
	return (write_uint(conv, args, 10, 0));
}

size_t	print_lowhex(t_conv conv, va_list args)
{
	return (write_uint(conv, args, 16, 0));
}

size_t	print_uphex(t_conv conv, va_list args)
{
	return (write_uint(conv, args, 16, 1));
}

size_t	print_octal(t_conv conv, va_list args)
{
	return (write_uint(conv, args, 8, 0));
}
