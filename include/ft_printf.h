/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_printf.h                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/05 19:30:44 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/12/09 19:40:56 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <unistd.h>
# include <math.h>
# include "libft.h"

# define FLAGS "#0-+ "
# define TYPES "csdiouxXpn"
# define NUMERIC "diouxX"

typedef struct	s_conv
{
	char		flags;
	size_t		width;
	size_t		prec;
	char		use_prec;
	char		length;
	char		type;
	size_t		written;
	int			fd;
}				t_conv;

/*
 ** ft_printf.c
*/
int				ft_printf(const char *format, ...);
int				ft_dprintf(int fd, const char *format, ...);
int				ft_vprintf(const char *format, va_list ap);
int				ft_vdprintf(int fd, const char *format, va_list ap);
size_t			handle_conv(char *start, va_list args, 
		size_t *written, int fd);

/*
 ** handle_conv.c
*/
size_t			handle_flags(char *start, t_conv *conv);
size_t			handle_width(char *start, t_conv *conv, va_list args);
size_t			handle_precision(char *start, t_conv *conv, va_list args);
size_t			handle_length(char *start, t_conv *conv);
size_t			handle_type(char *start, t_conv *conv);

/*
 ** misc1.c
*/
size_t			repeat_if(t_conv conv, char c, size_t amount, int condition);
size_t			write_if(t_conv conv, char *s, int condition);
size_t			write_padding(t_conv conv, size_t len, size_t extra, int rpad);
ssize_t			find_char(const char *s, char c);
size_t			write_ustr(t_conv conv, unsigned char *str, size_t len);

/*
 ** int1.c
*/
size_t			write_ullong(t_conv conv, unsigned long long n,
		size_t radix, int inupper);
void			get_int(t_conv conv, va_list args, long long *num);
void			get_uint(t_conv conv, va_list args, unsigned long long *num);
size_t			write_int(t_conv conv, va_list args,
		size_t radix, int isupper);
size_t			write_uint(t_conv conv, va_list args,
		size_t radix, int isupper);

/*
 ** int2.c
*/
size_t			ullong_len(unsigned long long n, size_t radix);
size_t			llong_len(long long n, size_t radix);
t_conv			*oct_prec(t_conv *conv, size_t len, int condition);
size_t			prefix_if(t_conv conv, size_t radix, int isupper, int cond);
size_t			write_sign(t_conv conv, int negative, int cond);

/*
 ** print_ints.c
*/
size_t			print_signed(t_conv conv, va_list args);
size_t			print_unsigned(t_conv conv, va_list args);
size_t			print_lowhex(t_conv conv, va_list args);
size_t			print_uphex(t_conv conv, va_list args);
size_t			print_octal(t_conv conv, va_list args);

/*
 ** print_misc.c
*/
size_t			print_unknown(t_conv conv, va_list args);
size_t			print_char(t_conv conv, va_list args);
size_t			print_str(t_conv conv, va_list args);
size_t			print_ptr(t_conv conv, va_list args);
size_t			print_n(t_conv conv, va_list args);

#endif
